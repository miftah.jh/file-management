# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/fileintegrator-mgtconfig-4.0.0-SNAPSHOT.jar /fileintegrator-mgtconfig.jar
# run application with this command line[
CMD ["java", "-jar", "/fileintegrator-mgtconfig.jar"]
